
/**
 * @file
 * Provides UI enhacements for date formatters add/edit form.
 *
 * @see date_formatters_form
 */

(function ($) {

// Namespace
Drupal.ArPHPDateFormatters = Drupal.ArPHPDateFormatters || {};

Drupal.ArPHPDateFormatters.configurationForm = function (form) {
  this.form = form;
  var that = this;

  $('input[name=format_type]', this.form).change(function () {
    if (that.isPHPDate()) {
      that.toPHPDate();
    }
    else if (that.isPHPCode()) {
      that.toPHPCode();
    }
    else {
      that.toNeutral();
    }
  }).change();
};

Drupal.ArPHPDateFormatters.configurationForm.prototype = {
  toPHPDate: function () {
    this.formItem($('#edit-php', this.form)[0]).hide();
    this.formItem($('#edit-php-date', this.form)[0]).show();
    this.formItem($('.arphp-mode', this.form)[0]).show();
  },

  toPHPCode: function () {
    this.formItem($('#edit-php-date', this.form)[0]).hide();
    this.formItem($('.arphp-mode', this.form)[0]).hide();
    this.formItem($('#edit-php', this.form)[0]).show();
  },

  toNeutral: function () {
    this.formItem($('#edit-php-date', this.form)[0]).show();
    this.formItem($('#edit-php', this.form)[0]).show();
    this.formItem($('.arphp-mode', this.form)[0]).show();
  },

  isPHPDate: function () {
    return $('input[name=format_type]:checked', this.form).val() == 'php_date';
  },

  isPHPCode: function () {
    return $('input[name=format_type]:checked', this.form).val() == 'php';
  },

  /**
   * Find the form-item for given element.
   */
  formItem: function (element) {
    // jQuery 1.3.x
    if (typeof $.fn.closest != 'undefined') {
      return $(element).closest('.form-item');
    }
    else {
      return $(element).parents('.form-item').slice(0, 1);
    }
  }
};

$(document).ready(function () {
  $('#arphp-date-form').each(function () {
    var form = new Drupal.ArPHPDateFormatters.configurationForm(this);
  });
});

})(jQuery);
