<?php

/**
 * @defgroup arphp_date Date formatters: Custom CCK date formatters that use
 * ArPHP for date formatting.
 *
 * Allows administrators to create custom CCK ArPHP date formatters.
 */

/**
 * @file
 *
 * Implements all functionality of this module.
 *
 * @TODO Use date.module date functions. This requires refactoring of ArDate.

 * @ingroup arphp_date
 */

/**
 * Implementation of hook_menu().
 */
function arphp_date_menu() {
  $items = array();

  $items['admin/settings/arphp-date'] = array(
    'title' => 'ArPHP Date formatters',
    'page callback' => 'arphp_date_list',
    'access arguments' => array('access administration pages'),
    'type' => MENU_NORMAL_ITEM,
  );

  $items['admin/settings/arphp-date/list'] = array(
    'title' => 'List',
    'page callback' => 'arphp_date_list',
    'access arguments' => array('access administration pages'),
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -5,
  );

  $items['admin/settings/arphp-date/add-formatter'] = array(
    'title' => 'Add date formatter',
    'page callback' => 'arphp_date_add_formatter',
    'access arguments' => array('access administration pages'),
    'type' => MENU_LOCAL_TASK,
  );

  $items['admin/settings/arphp-date/edit-formatter'] = array(
    'title' => t('Edit formatter'),
    'page callback' => 'arphp_date_edit_formatter',
    'access arguments' => array('access administration pages'),
    'type' => MENU_CALLBACK,
  );

  $items['admin/settings/arphp-date/enable-formatter'] = array(
    'title' => t('Enable formatter'),
    'page callback' => 'arphp_date_enable_formatter',
    'access arguments' => array('access administration pages'),
    'type' => MENU_CALLBACK,
  );

  $items['admin/settings/arphp-date/disable-formatter'] = array(
    'title' => t('Disable formatter'),
    'page callback' => 'arphp_date_disable_formatter',
    'access arguments' => array('access administration pages'),
    'type' => MENU_CALLBACK,
  );

  $items['admin/settings/arphp-date/delete-formatter'] = array(
    'title' => t('Delete formatter'),
    'page callback' => 'arphp_date_delete_formatter',
    'access arguments' => array('access administration pages'),
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Implementation of hook_theme().
 */
function arphp_date_theme() {
  $theme = array(
    'arphp_date_formatter_php_date' => array(
      'arguments' => array('element' => NULL, 'formatter' => NULL),
    ),
    'arphp_date_formatter_php' => array(
      'arguments' => array('element' => NULL, 'formatter' => NULL),
    ),
  );

  $result = db_query('SELECT * FROM {arphp_date_formatter} WHERE status > 0');
  while ($formatter = db_fetch_object($result)) {
    $theme['arphp_date_formatter_'. $formatter->machine_name] = array(
      'arguments' => array('element' => NULL),
      'function' => 'theme_arphp_date_formatter',
    );
  }

  return $theme;
}

/**
 * @defgroup arphp_date_callbacks Date formatters menu callbacks
 * @{
 */

/**
 * List defined custom date formatters.
 *
 * @ingroup arphp_date_callbacks
 */
function arphp_date_list() {
  $output = '';
  $now = $_SERVER['REQUEST_TIME'];

  $header = array(t('Label'), t('Format'), t('Preview'), t('Status'), t('Operations'));

  $rows = array();
  $result = pager_query('SELECT * FROM {arphp_date_formatter} ORDER BY weight, id DESC', 20, 0);
  while ($formatter = db_fetch_object($result)) {
    $rows[$formatter->id] = array();

    // Name
    $rows[$formatter->id][] = check_plain($formatter->label);

    // Format
    $rows[$formatter->id][] = '<code>'. ($formatter->format_type == 'php_date' ? check_plain($formatter->format_string) : l(t('PHP Code'), 'admin/settings/arphp-date/edit-formatter/'. $formatter->id)) .'</code>';

    // Example
    if ($formatter->format_type == 'php_date') {
      $Ar = new Arabic();
      $Ar->ArDate->setMode($formatter->mode);
      $rows[$formatter->id][] = $Ar->date($formatter->format_string, $now);
    }
    else if ($formatter->format_type == 'php') {
      $rows[$formatter->id][] = t('N/A');
    }

    // Status
    $rows[$formatter->id][] = $formatter->status ? t('Enabled') : t('Disabled');

    // Operations
    $operations = array();
    $operations[] = l(t('Edit'), 'admin/settings/arphp-date/edit-formatter/'. $formatter->id);
    $operations[] = $formatter->status ?
      l(t('Disable'), 'admin/settings/arphp-date/disable-formatter/'. $formatter->id) :
      l(t('Enable'), 'admin/settings/arphp-date/enable-formatter/'. $formatter->id);
    $operations[] = l(t('Delete'), 'admin/settings/arphp-date/delete-formatter/'. $formatter->id);
    $rows[$formatter->id][] = implode(' | ', $operations);
  }

  if (empty($rows)) {
    $rows[] = array(array('colspan' => 5, 'data' => t('There are no custom date formatters defined, !add your first.', array('!add' => l(t('add'), 'admin/settings/arphp-date/add')))));
  }

  $output .= theme('table', $header, $rows);
  $output .= theme('pager', NULL, 50, 0);

  return $output;
}

/**
 * Add a new custom date formatter.
 *
 * @ingroup arphp_date_callbacks
 */
function arphp_date_add_formatter() {
  return drupal_get_form('arphp_date_form');
}

/**
 * Edit a custom date formatter.
 *
 * @ingroup arphp_date_callbacks
 */
function arphp_date_edit_formatter($formatter_id) {
  $formatter = arphp_date_load_or_404($formatter_id);
  return drupal_get_form('arphp_date_form', $formatter);
}

/**
 * Enable a custom date formatter.
 *
 * @ingroup arphp_date_callbacks
 */
function arphp_date_enable_formatter($formatter_id) {
  $formatter = arphp_date_load_or_404($formatter_id);
  $formatter->status = 1;
  arphp_date_save($formatter);
  drupal_set_message(t('Date formatter %name enabled', array('%name' => $formatter->label)));
}

/**
 * Enable a custom date formatter.
 *
 * @ingroup arphp_date_callbacks
 */
function arphp_date_disable_formatter($formatter_id) {
  $formatter = arphp_date_load_or_404($formatter_id);
  $formatter->status = 0;
  arphp_date_save($formatter);
  drupal_set_message(t('Date formatter %name disabled', array('%name' => $formatter->label)));
}

/**
 * Delete a custom date formatter.
 *
 * @ingroup arphp_date_callbacks
 */
function arphp_date_delete_formatter($formatter_id) {
  $formatter = arphp_date_load_or_404($formatter_id);
  return drupal_get_form('arphp_date_delete_confirm_form', $formatter);
}
/**
 * @} End of "defgroup arphp_date_callbacks"
 */

/**
 * @defgroup arphp_date_forms Date formatters forms and submit handlers
 * @{
 */

/**
 * Returns a form for adding/editing a date formatter.
 *
 * @param $formatter
 *  A date formatter object.
 *
 * @return
 *  FAPI Form array.
 *
 * @ingroup arphp_date_forms
 */
function arphp_date_form(&$form_state, $formatter = FALSE) {
  drupal_add_js(drupal_get_path('module', 'arphp_date') .'/arphp_date.js');
  $form = array();

  if ($formatter === FALSE) {
    $formatter = arphp_date_default();
  }

  $form['#formatter'] = $formatter;

  // Label
  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => $formatter->label,
    '#description' => t('Human readable label which will be the identifier of this formatter in the UI.'),
    '#required' => TRUE,
  );

  // Machine name
  // @TODO An option to auto-generate this.
  $form['machine_name'] = array(
    '#title' => t('Internal name'),
    '#type' => 'textfield',
    '#default_value' => $formatter->machine_name,
    '#description' => t('Machine-readable name used for internal storage and communication with the CCK API. A good practice is to make this the same as the label but all lower case, and with underscores instead of spaces.'),
    '#required' => TRUE,
  );

  // Group: Format
  $form['format'] = array(
    '#title' => t('Format'),
    '#type' => 'fieldset',
  );

  // Format type
  $form['format']['format_type'] = array(
    '#title' => t('Format type'),
    '#type' => 'radios',
    '#options' => array('php_date' => t('PHP Date format'), 'php' => t('PHP Code')),
    '#default_value' => $formatter->format_type,
    '#description' => t('Identify which format type to use. %php_date is a string formatted using PHP\'s date function, see !date for details. %php is PHP code which recieves a $date object, and should return it formatted.', array('%php_date' => t('PHP Date format'), '%php' => t('PHP Code'), '!date' => l('http://php.net/date', 'http://php.net/date'))),
  );

  // PHP Date
  $form['format']['php_date'] = array(
    '#title' => t('Format'),
    '#type' => 'textfield',
    '#default_value' => $formatter->format_type == 'php_date' ? $formatter->format_string : '',
    '#description' => t('PHP Date format string. See !date for details.', array('!date' => l('http://php.net/date', 'http://php.net/date'))),
  );

  $form['format']['mode'] = array(
    '#title' => t('Calendar'),
    '#type' => 'radios',
    '#options' => arphp_date_modes(),
    '#default_value' => $formatter->mode,
    '#attributes' => array('class' => 'arphp-mode'),
  );

  // PHP Code
  $form['format']['php'] = array(
    '#title' => t('PHP Code'),
    '#type' => 'textarea',
    '#default_value' => $formatter->format_type == 'php' ? $formatter->format_string : '',
    '#rows' => 5,
    '#description' => t('PHP Code which returns the formatted date. Use !php tags. You have the following variables: !variables. This is basically the body of the formatter as called by CCK.', array('!variables' => '$formatter, $element, $ArPHP', '!php' => '&lt;?php ?&gt;')),
  );

  // Group: Status and order
  $form['more'] = array(
    '#title' => t('Status and ordering'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  // Status
  $form['more']['status'] = array(
    '#title' => t('Status'),
    '#type' => 'radios',
    '#options' => array(0 => t('Disabled'), 1 => t('Enabled')),
    '#default_value' => $formatter->status,
    '#required' => TRUE,
  );

  // Weight
  $form['more']['weight'] = array(
    '#title' => t('Weight'),
    '#type' => 'weight',
    '#default_value' => $formatter->weight,
    '#required' => TRUE,
  );

  // Group: Buttons
  $form['buttons'] = array(
    '#prefix' => '<hr />',
  );

  // Create/Update
  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => $formatter->id ? t('Update') : t('Create'),
  );

  return $form;
}

/**
 * Validates form-submitted date formatter.
 *
 * @ingroup arphp_date_forms
 * @see arphp_date_form
 */
function arphp_date_form_validate(&$form, &$form_state) {
  $form_values = $form_state['values'];

  // Unique machine name
  if ($formatter_id = db_result(db_query('SELECT id FROM {arphp_date_formatter} WHERE machine_name = "%s" AND id != %d', $form_values['machine_name'], $form['#formatter']->id))) {
    form_set_error('machine_name', t('A date formatter with the same internal name exists, !edit, or enter another internal name for this formatter', array('!edit' => l(t('edit it'), 'admin/settings/arphp-date/edit-formatter/'. $formatter_id))));
  }

  // Format string
  if ($form_values['format_type'] == 'php_date' && empty($form_values['php_date'])) {
    form_set_error('php_date', t('!name field is required.', array('!name' => t('Format'))));
  }
  else if ($form_values['format_type'] == 'php' && empty($form_values['php'])) {
    form_set_error('php', t('!name field is required.', array('!name' => t('PHP Code'))));
  }
}

/**
 * Inserts or updates a date formatter submitted through the form.
 *
 * @ingroup arphp_date_forms
 * @see arphp_date_form
 */
function arphp_date_form_submit(&$form, &$form_state) {
  $form_values = $form_state['values'];
  $formatter = $form['#formatter'];

  if ($formatter->id) {
    $form_values['id'] = $formatter->id;
  }

  if ($form_values['format_type'] == 'php_date') {
    $form_values['format_string'] = $form_values['php_date'];
  }
  else {
    $form_values['format_string'] = $form_values['php'];
  }

  $new_formatter = (object) $form_values;
  arphp_date_save($new_formatter);

  // Clear CCK and Views cache.
  content_clear_type_cache();

  // Rebuild Theme Registry
  // @TODO Fix this, try to work around this. Theme rebuilding is too heavy.
  drupal_rebuild_theme_registry();

  if ($formatter->id) {
    drupal_set_message(t('ArPHP Date formatter %name updated', array('%name' => $new_formatter->label)));
  }
  else {
    drupal_set_message(t('ArPHP Date formatter %name created', array('%name' => $new_formatter->label)));
  }

  $form_state['redirect'] = 'admin/settings/arphp-date';
}

/**
 * Returns a form for confirming the delete of a date formatter.
 *
 * @param $formatter
 *  A date formatter object.
 *
 * @return
 *  FAPI Form array.
 *
 * @ingroup arphp_date_forms
 */
function arphp_date_delete_confirm_form(&$form_state, $formatter) {
  $form['#formatter'] = $formatter;

  $form = confirm_form(
    $form,
    t('Are you sure want to delete date formatter %name', array('%name' => $formatter->label)),
    'admin/settings/arphp-date',
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
  return $form;
}

/**
 * Deletes a date formatter after the operation was confirmed.
 *
 * @ingroup arphp_date_forms
 * @see arphp_date_delete_confirm_form
 */
function arphp_date_delete_confirm_form_submit(&$form, &$form_state) {
  $formatter = $form['#formatter'];
  arphp_date_delete($formatter);
  drupal_set_message(t('Date formatter %name deleted', array('%name' => $formatter->label)));
  $form_state['redirect'] = 'admin/settings/arphp-date';
}
/**
 * @} End of "defgroup arphp_date_forms"
 */

/**
 * @defgroup arphp_date_cck CCK integration
 * @{
 * Hooks and other helper functions which integrate date formatters to CCK.
 */

/**
 * Implementation of hook_field_formatter_info().
 */
function arphp_date_field_formatter_info() {
  $formatters = array();

  $result = db_query('SELECT * FROM {arphp_date_formatter} WHERE status > 0');
  while ($formatter = db_fetch_object($result)) {
    $formatters[$formatter->machine_name] = array(
      'label' => $formatter->label,
      'field types' => array('date', 'datestamp'),
    );
  }

  return $formatters;
}

/**
 * A date formatter based on php's date function.
 */
function arphp_date_php_date_formatter($formatter, $field, $item, $node) {
  $field['output_format_custom'] = $formatter->format_string;
  return arphp_date_date_field_formatter($field, $item, 'default', $node);
}

/**
 * Main entry point for all ArPHP Date formatters. Loads the formatter and calls
 * the appropriate theme function.
 *
 * Thanks to the new, bad, formatters API this has to be a theme function.
 */
function theme_arphp_date_formatter($element) {
  $formatter = db_fetch_object(db_query('SELECT * FROM {arphp_date_formatter} WHERE machine_name = "%s"', $element['#formatter']));

  return theme('arphp_date_formatter_'. $formatter->format_type, $element, $formatter);
}

/**
 * A date formatter which uses allows eval()ing PHP code on runtime to format
 * the given date.
 */
function theme_arphp_date_formatter_php($element, $formatter) {
  $ArPHP = new Arabic();
  $ArPHP->ArDate->setMode($formatter->mode);

  ob_start();
  echo eval('?>'. $formatter->format_string);
  $output = ob_get_contents();
  ob_end_clean();

  return $output;
}

/**
 * A date formatter based on ArPHP's date function.
 */
function theme_arphp_date_formatter_php_date($element, $formatter) {
  $time = strtotime($element['#item']['value']);

  $Ar = new Arabic();
  $Ar->ArDate->setMode($formatter->mode);
  return $Ar->date($formatter->format_string, $time);
}
/**
 * @} End of "defgroup arphp_date_cck"
 */

/**
 * @defgroup arphp_date_crud CRUD operations for date formatters
 * @{
 * Implements Create, Retrieve, Update and Delete operations for
 * date formatters
 */

/**
 * Loads the date formatter with given ID or fallback to drupal_not_found().
 *
 * @ingroup arphp_date_crud
 */
function arphp_date_load_or_404($formatter_id) {
  $formatter = arphp_date_load($formatter_id);
  if (!$formatter) {
    return drupal_not_found();
  }
  return $formatter;
}

/**
 * Loads the date formatter with given ID.
 *
 * @ingroup arphp_date_crud
 */
function arphp_date_load($formatter_id) {
  return db_fetch_object(db_query('SELECT * FROM {arphp_date_formatter} WHERE id = %d', $formatter_id));
}

/**
 * Saves given date formatter, supports both INSERT/UPDATE transparently.
 *
 * @ingroup arphp_date_crud
 */
function arphp_date_save($formatter) {
  if ($formatter->id) {
    drupal_write_record('arphp_date_formatter', $formatter, 'id');
  }
  else {
    drupal_write_record('arphp_date_formatter', $formatter);
  }
}

/**
 * Delete given date formatter
 *
 * @ingroup arphp_date_crud
 */
function arphp_date_delete($formatter) {
  db_query('DELETE FROM {arphp_date_formatter} WHERE id = %d', $formatter->id);
}

/**
 * Returns a new date formatter object.
 *
 * @see arphp_date_form
 */
function arphp_date_default() {
  $formatter = new stdClass();

  $formatter->id = 0;
  $formatter->label = '';
  $formatter->machine_name = '';
  $formatter->format_type = 'php_date';
  $formatter->format_string = '';
  $formatter->mode = 1;
  $formatter->weight = 0;
  $formatter->status = 1;

  return $formatter;
}
/**
 * @} End of "defgroup arphp_date_crud"
 */

/**
 * @defgroup arphp_date_date_api Date formatters wrappers around Date API 
 * @{
 * Implements Date API knock-offs which are mostly the same as Date API but with
 * a bit more customizability.
 */

/**
 * Calls arphp_date_date_formatter_process() instead of
 * date_formatter_process().
 *
 * @see date_field_formatter()
 */
function arphp_date_date_field_formatter($field, $item, $formatter, $node) {
  // Only display the start date for a repeating date
  // on the node itself unless we're showing all dates.
  if (arg(0) == 'node' && arg(1) == $node->nid // Make sure we're looking at the node 
    && empty($node->date_id) && empty($node->date_repeat_show) // Make sure no date was specifically requested
    && module_exists('date_repeat') && $field['repeat'] == 1) {
    $node->date_id = 'date:'. $node->nid .':'. $field['field_name'] .':0:0';
  }
  // If we're trying to display only a single date on a node, see if
  // this is the right one.
  if (isset($node->date_id) && isset($item['#delta'])) {
    list($module, $nid, $field_name, $delta, $other) = explode(':', $node->date_id);
    if ($field_name != $field['field_name'] || $delta != $item['#delta']) {
      return;
    }
  }
  // Call the right theme for this formatter.
  $dates = arphp_date_date_formatter_process($field, $item, $node, $formatter);
  if ($formatter != 'format_interval') {
    $output = theme('date_display_combination', $field, $item, $dates, $node);
  }
  else {
    $output = theme('date_format_interval', $field, $item, $dates, $node);
  }
  return $output;
}

/**
 * Calls arphp_date_date_formatter_format() instead of
 * date_formatter_format() passing it a $field object instead of a field name.
 *
 * @see date_formatter_process()
 */
function arphp_date_date_formatter_process($field, $item, $node, $formatter = 'default') {
  $dates = array();
  $timezone = date_default_timezone_name();
  if (!is_array($field) || !is_array($item) || empty($timezone)) {
    return $dates;
  }
  
  $format = arphp_date_date_formatter_format($formatter, $field);
  $process = date_process_values($field);
  foreach ($process as $processed) {
    if (empty($item[$processed])) {
      $dates[$processed] = NULL;
    }
    else {
      // create a date object with a gmt timezone from the database value
      $value = $item[$processed];
      if ($field['type'] == DATE_ISO) {
        $value = str_replace(' ', 'T', date_fuzzy_datetime($value));
      }
      $date = date_make_date($value, 'UTC', $field['type']);
      $dates[$processed] = array();
      $dates[$processed]['db']['object'] = $date;
      $dates[$processed]['db']['datetime'] = date_format($date, DATE_FORMAT_DATETIME);

      // For no timezone handling, set local value to the same as the db value.
      if (!date_timezone_convert($field, $item[$processed])) {
          $dates[$processed]['local'] = $dates[$processed]['db'];
      }
      else {
        $timezone = date_get_timezone($field['tz_handling'], $item['timezone']);
        date_timezone_set($date, timezone_open($timezone));
        $dates[$processed]['local']['object'] = $date;
        $dates[$processed]['local']['datetime'] = date_format($date, DATE_FORMAT_DATETIME);
        $dates[$processed]['local']['timezone'] = $timezone;
        $dates[$processed]['local']['offset'] = date_offset_get($date);
      }

      //format the date, special casing the 'interval' format which doesnt need to be processed
      $dates[$processed]['formatted'] = '';
      if (is_object($date)) {
        if ($format == 'format_interval') {
          $dates[$processed]['interval'] = date_format_interval($date);
        } 
        elseif (!empty($format)) {
          $dates[$processed]['formatted'] = date_format_date($date, 'custom', $format);
          $dates[$processed]['formatted_date'] = date_format_date($date, 'custom', date_limit_format($format, array('year', 'month', 'day')));
          $dates[$processed]['formatted_time'] = date_format_date($date, 'custom', date_limit_format($format, array('hour', 'minute', 'second')));
          $dates[$processed]['formatted_timezone'] = date_format_date($date, 'custom', date_limit_format($format, array('timezone')));
          
        }  
      }
    }
  }
  if (empty($dates['value2'])) {
    $dates['value2'] = $dates['value'];
  }
  $date1 = $dates['value']['local']['object'];
  $date2 = $dates['value2']['local']['object'];
  $all_day1 = theme('date_all_day', 'date1', $date1, $date2, $format, $node);
  $all_day2 = theme('date_all_day', 'date2', $date1, $date2, $format, $node);
  if ((!empty($all_day1) && $all_day1 != $dates['value']['formatted']) 
  || (!empty($all_day2) && $all_day2 != $dates['value2']['formatted'])) {
    $dates['value']['formatted_time'] = theme('date_all_day_label');
    $dates['value2']['formatted_time'] = theme('date_all_day_label'); 
    $dates['value']['formatted'] = $all_day1;
    $dates['value2']['formatted'] = $all_day2;
  }
  $dates['format'] = $format;
  return $dates;
}

/**
 * Accepts a $field object instead of a $field_name. 
 *
 * @see date_formatter_format().
 */
function arphp_date_date_formatter_format($formatter, $field) {
  // Any field might want to display the timezone name.
  $field['granularity'][] = 'timezone';
  switch ($formatter) {
    case 'ical':
      return 'Ymd\THis';
    case 'timestamp':
      return 'U';
    case 'iso':
      return DATE_FORMAT_ISO .'P';
    case 'feed':
      return 'D, j M Y H:i:s O';
    case 'format_interval':
      return 'format_interval';

    case 'long':
    case 'medium':
    case 'short':
    case 'default':
      $custom = 'output_format_custom'. ($formatter != 'default' ? '_'. $formatter : '');
      $value = 'output_format_date'. ($formatter != 'default' ? '_'. $formatter : '');
      if ($field[$custom] > '') {
        $format = $field[$custom];
      }
      elseif ($field[$value]) {
        $format = $field[$value];
      }
      else {
        switch ($formatter) {
          case 'long':
            $format = variable_get('date_format_long',  'l, F j, Y - H:i');
            break;
          case 'medium':
            $format = variable_get('date_format_medium',  'D, m/d/Y - H:i');
            break;
          default:
            $format = variable_get('date_format_short', 'm/d/Y - H:i');
            break;
        }
      }
      break;
  }
  return date_limit_format($format, date_granularity($field));
}
/**
 * @} End of "defgroup arphp_date_date_api"
 */

/**
 * @defgroup arphp_date_unused Unused functions.
 * @{
 * Unused functions which are pending removal or completion of implementation.
 */

/**
 * Implementation of hook_form_FORMID_alter().
 *
 * @TODO This is currently unused because there is no way to override
 * format_date().
 */
function arphp_date_form_system_date_time_settings_alter(&$form, &$form_state) {
  $Ar = new Arabic();
  $now = time();

  foreach (array('date_format_short', 'date_format_medium', 'date_format_long') as $date_preset) {
    $options = array_merge($form['date_formats'][$date_preset]['#options'], arphp_date_formats($date_preset));

    foreach (arphp_date_modes() as $mode => $name) {
      $Ar->ArDate->setMode($mode);

      $arabic_options = array();
      foreach ($options as $format => $option) {
        $arabic_options['arphp_'. $mode .':'. $format] = $Ar->date($format, $now);
      }

      $form['date_formats'][$date_preset]['#options'] += $arabic_options;
    }
  }
}

/**
 * Returns available ArPHP date modes.
 */
function arphp_date_modes($mode = NULL) {
  $modes = array(
    1 => t('Islamic'),
    2 => t('Julian'),
    3 => t('Gregorian'),
    4 => t('Gregorian/Julian'),
  );

  if ($mode !== NULL) {
    return $modes[$mode];
  }

  return $modes;
}

/**
 * Return extra date formats that make sense for Arabic dates.
 *
 * @TODO This is the base for user-configurable custom arabic date formats.
 */
function arphp_date_formats($preset = NULL) {
  $formats = array(
    'date_format_long' => array(
      'l dS F Y h:i:s A' => t('Format from ArPHP example on its website'),
    ),
  );

  if ($preset !== NULL) {
    return isset($formats[$preset]) ? $formats[$preset] : array();
  }

  return $formats;
}
/**
 * @} End of "defgroup arphp_date_unused"
 */
