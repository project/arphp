
= Installation =

* Unpack the project files onto your modules directory (usually 'sites/all/modules')
* Download the latest Ar-PHP library from  http://ar-php.org The module will only work with 2 or higher.
* Unpack the Ar-PHP library inside the module's directory or enter the path to the library on your server in the module configuration page. 
* Enable the module on the modules page ('http://example.com/?q=admin/build/modules')
